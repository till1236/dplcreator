function sendRequest(_method, _url, _parameter, _onSucces, _onError)
{
    var http = new XMLHttpRequest();
    var params = "";
    for(var i = 0; i < _parameter.length; i++)
    {
        params = params + _parameter[i].variable + "=" + _parameter[i].value;
        if(i < _parameter.length-1)
            params = params + "&";
    }
    http.open(_method, _url, true);

//Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    http.onreadystatechange = function() {//Call a function when the state changes.
        if(http.readyState == 4 && http.status == 200) {
            _onSucces(http);
        }
        else if(http.status != 200)
        {
            alert("error status: ("+ http.status + "), response: (" + http.responseText + ") rdyState: (" + http.readyState);
            _onError(http);
        }
    };
    http.send(params);
}
