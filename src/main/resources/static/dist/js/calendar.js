var $sked1 = null;

var today = null;
var tomorrow = null;
var yesterday = null;

var addEvent = null;

function initSked(theList, theList2, mydate)
{
    today = function(hours, minutes) {
        var date = new Date(mydate);
        date.setHours(hours, minutes, 0, 0);
        return date;
    }
    yesterday = function(hours, minutes) {
        var date = today(hours, minutes);
        date.setTime(date.getTime() - 24 * 60 * 60 * 1000);
        return date;
    }
    tomorrow = function(hours, minutes) {
        var date = today(hours, minutes);
        date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
        return date;
    }
    var locations = [];
    for (var i = 0; i < theList.length; i++) {
        locations.push({id: theList[i].id, name: theList[i].name, tzOffset: 7 * 60});
    }
    var events = [];
    for (var i = 0; i < theList2.length; i++) {
        var startText = "1995-12-17T"+theList2[i].start;
        var endText = "1995-12-17T"+theList2[i].end;

        var _start = new Date(startText);
        var _end =  new Date(endText);

        var rs = today(_start.getHours(), _start.getMinutes());
        var rend = today(_end.getHours(), _end.getMinutes());

        if(rs > rend) {
            rend = tomorrow(_end.getHours(), _end.getMinutes());
        }
        var emp = theList2[i]["employee"];
        var realid = theList2[i].id;
        var loc = theList2[i]["location"];
        if(loc != null)
        {
            events.push({
                name: emp != null ? emp ["prename"] : "",
                location: loc.id,
                start: rs,
                end: rend,
                userData: realid
            });
        }
    }

    $sked1 = $('#sked1').skedTape({
        caption: 'Shifts',
        start: today(7, 0),
        end: tomorrow(7, 0),
        showEventTime: true,
        showEventDuration: true,
        scrollWithYWheel: true,
        locations: locations.slice(),
        events: events.slice(),
        maxTimeGapHi: 60 * 1000, // 1 minute
        minGapTimeBetween: 1 * 60 * 1000,
        snapToMins: 1,
        editMode: false,
        timeIndicatorSerifs: true,
        showIntermission: true,
        allowCollisions: true,
        handleEventContextMenu: function(e){return ContextMenu(e)},
        formatters: {
            date: function (date) {
                return $.fn.skedTape.format.date(date, 'l', '.');
            },
            duration: function (ms, opts) {
                return $.fn.skedTape.format.duration(ms, {
                    hrs: 'h.',
                    min: 'min.'
                });
            },
        },
        canAddIntoLocation: function(location, event) {
            return true;
        },
        postRenderLocation: function($el, location, canAdd) {
            this.constructor.prototype.postRenderLocation($el, location, canAdd);
            $el.prepend('<i class="fas fa-thumbtack text-muted"/> ');
        }
    });

    $sked1.on('event:click.skedtape', function(e) {
        //$sked1.skedTape('removeEvent', e.detail.event.id);

        $("#new-event-start").val(formatDate(e.detail.event.start));
        $("#new-event-end").val(formatDate(e.detail.event.end));
        $("#new-event-location").val(e.detail.event.location);
    });

    $sked1.on('event:contextmenu.skedtape', function(e) {
        //$sked1.skedTape('removeEvent', e.detail.event.id);

        return ContextMenu();
    });

    $sked1.on('intersection:click.skedtape', function(e) {
        //$sked1.skedTape('removeEvent', e.detail.event.id);

        $("#new-event-start").val(formatDate(e.detail.event.start));
        $("#new-event-end").val(formatDate(e.detail.event.end));
        $("#new-event-location").val(e.detail.event.location);
    });

    var el = document.getElementsByClassName("mydiv");
    for(var i = 0; i < el.length; i++)
    {
        dragElement(el.item(i));
    }

    document.getElementById("repeat").checked = false;

    addEvent = function() {


        var parameter = [{
            variable: "start1",
            value: $("#new-event-start").val()
        },
            {
                variable: "end",
                value: $("#new-event-end").val()
            },
            {
                variable: "location",
                value: $("#new-event-location").val()
            },
            {
                variable: "date",
                value: mydate
            }];

        if(document.getElementById("repeat").checked)
        {

            var params ="";
            params += (document.getElementById("repeat-monday").checked ? "1" : "0") +"-";
            params += (document.getElementById("repeat-tuesday").checked ? "1" : "0") +"-";
            params += (document.getElementById("repeat-wednesday").checked ? "1" : "0") +"-";
            params += (document.getElementById("repeat-thursday").checked ? "1" : "0") +"-";
            params += (document.getElementById("repeat-friday").checked ? "1" : "0") +"-";
            params += (document.getElementById("repeat-saturday").checked ? "1" : "0") +"-";
            params += (document.getElementById("repeat-sunday").checked ? "1" : "0");

            parameter.push({variable: "repeat", value: params});
        }


        sendRequest("POST", "event/add", parameter,
            function(http){
                var obj = JSON.parse(http.responseText);
                if(obj == null)
                    return;

                var startText = "1995-12-17T"+obj.start;
                var endText = "1995-12-17T"+obj.end;

                var _start = new Date(startText);
                var _end =  new Date(endText);

                var rs = today(_start.getHours(), _start.getMinutes());
                var rend = today(_end.getHours(), _end.getMinutes());

                if(rs > rend) {
                    rend = tomorrow(_end.getHours(), _end.getMinutes());
                }

                $sked1.skedTape('addEvent', {
                    start: rs,
                    end: rend,
                    name: obj.employee == null ? '' : obj.employee.prename,
                    location: obj.location.id,
                    userData: obj.id
                });
            },
            function (http) {

            });
    }
}

function showRepeatPanel(e)
{
    document.getElementById("repeatdays").style.display = document.getElementById("repeat").checked ? "flex" : "none";
}

$(document).bind('contextmenu click',function(){
    $(".context-menu").hide();
    $("#txt_id").val("");
});

function ContextMenu(e)
{
    var top = e.pageY+5;
    var left = e.pageX;

    // Show contextmenu
    $(".context-menu").toggle(100).css({
        top: top + "px",
        left: left + "px"
    });
    var ids = e.currentTarget.getAttribute('data-value');
    $("#currentEventID").val(ids);
    return false;
}

function updateEvent()
{
    sendRequest("POST", "event/update", [{
            variable: "eventId",
            value: $("#currentEventID").val().split("-")[1]
        },{
            variable: "date",
            value: $("#event_date").val()
        },{
            variable: "start1",
            value: $("#event_start_time").val()
        },{
            variable: "end",
            value: $("#event_end_time").val()
        },{
            variable: "employeeId",
            value: $("#event_employee").val()
        },{
            variable: "location",
            value: $("#event_location").val()
        }],
        function(http){
            var obj = JSON.parse(http.responseText);
            if(obj == null)
                return;
            var startText = "1995-12-17T"+obj.start;
            var endText = "1995-12-17T"+obj.end;
            var _start = new Date(startText);
            var _end =  new Date(endText);

            var rs = today(_start.getHours(), _start.getMinutes());
            var rend = today(_end.getHours(), _end.getMinutes());

            if(rs > rend) {
                rend = tomorrow(_end.getHours(), _end.getMinutes());
            }
            $sked1.skedTape("removeEvent", $("#currentEventID").val().split("-")[0]);
            $sked1.skedTape("addEvents", [{
                name: obj.employee == null ? "" : obj.employee.prename,
                location: obj.location.id,
                start: rs,
                end: rend,
                userData: obj.id
            }]);
            closeSettings();
        },
        function(http){

        });
}

function closeSettings()
{
    document.getElementById("shift-settings-menu").style.display = 'none';
}

function actualEventSettings()
{
    sendRequest("POST", "event/info", [{
            variable: "eventId",
            value: $("#currentEventID").val().split("-")[1],
        }],
        function(http){
            var obj = JSON.parse(http.responseText);
            if(obj == null)
                return;
            var dt = new Date(obj.date);
            var dd = dt.getDate();
            var mm = dt.getMonth()+1; //January is 0!
            var yyyy = dt.getFullYear();

            if(dd<10) {
                dd = '0'+dd
            }

            if(mm<10) {
                mm = '0'+mm
            }

            var today1 = yyyy + '-' + mm + '-' + dd;
            $("#event_date").val(today1);
            $("#event_start_time").val(obj.start);
            $("#event_end_time").val(obj.end);
            $("#event_location").val(obj.location.id);
            $("#event_employee").val(obj.employee != null ? obj.employee.id:0);

            document.getElementById("shift-settings-menu").style.display = 'inline';
            document.getElementById("shift-settings-menu").style.position = 'absolute';
            var left = (screen.width - $("#shift-settings-menu").width()) / 2;
            var top = (screen.height - $("#shift-settings-menu").height()) / 4;
            document.getElementById("shift-settings-menu").style.left = left + 'px';
            document.getElementById("shift-settings-menu").style.top = top + 'px';
        },
        function(http){

        });
}

function deleteEvent()
{
    sendRequest("POST", "event/delete", [{
            variable: "eventId",
            value: $("#currentEventID").val().split('-')[1],
        }],
        function(http){
            $sked1.skedTape('removeEvent', $("#currentEventID").val().split('-')[0]);
        },
        function(http){

        });

}

function formatDate(date)
{
    var h = date.getHours();
    var m = date.getMinutes();
    var ret = "";
    if(h == 0)
    {  ret= "00";}
    else if(h < 10)
    {  ret="0"+h.toString();}
    else
    {  ret =h.toString();}

    ret= ret+":";
    if(m == 0)
    {  ret=ret+"00";}
    else if(m < 10)
    { ret=ret+"0"+m.toString();}
    else
    { ret=ret+m.toString();}
    return ret;
}

function addLocation(){

    sendRequest("POST", "/location/add",[{
            variable: "name",
            value: $("#new-location-name").val()
        }], function(http){
            var obj = JSON.parse(http.responseText);
            if(obj == null)
                return;
            var oopt = document.createElement("option");

            oopt.value = obj.id;
            oopt.text = $("#new-location-name").val();
            document.getElementById("new-event-location").add(oopt);

            $sked1.skedTape('addLocations', [
                {id: http.responseText, name: $("#new-location-name").val(), tzOffset: 7 * 60}
            ]);
        },
        function(http){

        });
}


function dragElement(elmnt) {

    var old_posX = 0, old_posY = 0;
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    var el = document.getElementsByClassName(elmnt.class + "header");
    for(var i = 0; i < el.length; i++)
    {
        el.item(i).onmousedown =dragMouseDown;
    }

    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;

        elmnt.style.position = "absolute";
        elmnt.style.left = pos3;
        elmnt.style.top = pos4;
        //elemt.ondragend =
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";



    }

    function closeDragElement(e) {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
        elmnt.style.top = "0";
        elmnt.style.left = "0";
        elmnt.style.position = "relative";
        elmnt.style.display ="none";
        var elementMouseIsOver = document.elementFromPoint(e.clientX, e.clientY);
        var currentEvent = elementMouseIsOver.getAttribute("data-value");
        var split = currentEvent.toString().split('-');


        if(currentEvent != null || currentEvent != "") {

            sendRequest("POST", "/event/setemployee", [
                {
                    variable: "shiftId",
                    value: split[1]
                },{
                    variable: "employeeId",
                    value: elmnt.getAttribute('data-value')
                }],function(http){

                var ev = JSON.parse(http.responseText);
                if(ev == null)
                    return;
                var startText = "1995-12-17T"+ev.start;
                var endText = "1995-12-17T"+ev.end;

                var _start = new Date(startText);
                var _end =  new Date(endText);

                var rs = today(_start.getHours(), _start.getMinutes());
                var rend = today(_end.getHours(), _end.getMinutes());

                if(rs > rend) {
                    rend = tomorrow(_end.getHours(), _end.getMinutes());
                }
                var emp = ev.employee;
                var realid = ev.id;
                $sked1.skedTape("removeEvent", split[0]);
                $sked1.skedTape("addEvent",{
                    name: emp != null ? emp.prename : "",
                    location: ev["location"]["id"].toString(),
                    start: rs,
                    end: rend,
                    userData: realid
                });
            },function(http){

            });
        }
        elmnt.style.display ="inline";
    }
}

