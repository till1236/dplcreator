package dplcreator.controller;

import dplcreator.dpl.entity.*;
import dplcreator.dpl.repository.*;
import dplcreator.security.SecurityServiceImpl;
import dplcreator.user.User;
import dplcreator.user.UserDetailsServiceImpl;
import dplcreator.user.UserRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
public class CompanyController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShiftTemplateRepository shiftTemplateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private SelectedCompanyService selectedCompanyService;

    @Autowired
    private SecurityServiceImpl securityService;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ActualShiftRepository actualShiftRepository;

    @Autowired
    private ShiftBlacklistRepository shiftBlacklistRepository;

    @GetMapping("/company")
    public String showCompany(Model model, @RequestParam(required = true) Long CompanyID)
    {
        Object user  = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(user instanceof User) {
            Optional<Employee> emp = employeeRepository.findEmployeeByUser((User) user);
            Optional<Company> company = companyRepository.findById(CompanyID);
            if(emp.isEmpty() || company.isEmpty())
                return "redirect:/index";

            Boolean found = false;
            for(Map.Entry<Employee, CompanyRole> u : company.get().getEmployees().entrySet())
            {
                if(emp.isPresent() && u.getKey().getId()== emp.get().getId())
                    found = true;

            }
            if(company.isEmpty() || !found)
            {
                return"redirect:/index";
            }
            SelectedCompany selectedCompany = selectedCompanyService.getSelectedCompanyByUser((User) user);
            if(selectedCompany != null)
                selectedCompanyService.removeSelectedCompany(selectedCompany);
            selectedCompanyService.addSelectedCompany(new SelectedCompany((User)user, company.get()));
            model.addAttribute("mycompany", company.get());
            return "company";
        }
        return "redirect:/index";
    }

    @GetMapping("/companies")
    public String showCompanies(Model model)
    {
        model.addAttribute("companies", companyRepository.findAll());
        return "companies";
    }

    @PostMapping("/company/search")
    @ResponseBody
    public List<Company> searchCompany(Model model, @RequestParam String text)
    {
        Object user  = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Company> out = new ArrayList<>();
        if(user instanceof User) {
            Optional<Employee> emp = employeeRepository.findEmployeeByUser((User) user);
            if (!emp.isPresent())
                return out;
            Employee employee = emp.get();

            out = companyRepository.findAllByNameIsContainingAndEmployeesNotContaining(text, employee);
        }

        return out;
    }

    @PostMapping("/company/userEnter")
    @ResponseBody
    public Boolean addEmployee(Model model, @RequestParam Long companyId)
    {
        Optional<Company> com = companyRepository.findById(companyId);
        if(!com.isPresent())
            return false;

        Company company = com.get();
        Object user  = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(user instanceof User) {
            Optional<Employee> emp = employeeRepository.findEmployeeByUser((User)user);
            if(!emp.isPresent())
                return false;
            Employee employee = emp.get();

            if(company.hasEmployee(employee))
                return false;

            Set<Employee> applications = company.getApplications();
            applications.add(employee);
            company.setApplications(applications);
            company = companyRepository.save(company);
            return true;
        }
        return false;
    }

    @GetMapping("/company/add")
    public String addCompany(Model model)
    {
        return "company_add";
    }

    @PostMapping("/company/add")
    public String addCompany(Model model, @RequestParam String companyName)
    {
        Company company = new Company(companyName);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Employee> emp = employeeRepository.findEmployeeByUser(user);
        if(emp.isPresent())
            company.addEmployee(emp.get(), CompanyRole.ADMIN);

        companyRepository.save(company);
        return "redirect:/index";
    }

    @PostMapping("/employee/add")
    public String addCompany(Model model, @RequestParam String empPrename, @RequestParam String empLastname)
    {
        Object user  = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if(user instanceof User) {
            SelectedCompany sc = selectedCompanyService.getSelectedCompanyByUser((User) user);

            if(sc == null)
            {
                return null;
            }
            //TODO: EMAIL
            Employee employee = new Employee(empPrename, empLastname, "");
            employee = employeeRepository.save(employee);
            Company company = sc.company;
            company.addEmployee(employee, CompanyRole.MEMBER);
            company = companyRepository.save(company);

            return "redirect:/company?CompanyID="+company.getId().toString();

        }
        return "redirect:/index";
    }


}
