package dplcreator.controller;

import dplcreator.dpl.entity.*;
import dplcreator.dpl.entity.Calendar;
import dplcreator.dpl.repository.*;
import dplcreator.security.SecurityService;
import dplcreator.security.SecurityServiceImpl;
import dplcreator.user.User;
import dplcreator.user.UserDetailsServiceImpl;
import dplcreator.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
class IndexController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShiftTemplateRepository shiftTemplateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private SelectedCompanyService selectedCompanyService;

    @Autowired
    private SecurityServiceImpl securityService;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ActualShiftRepository actualShiftRepository;

    @Autowired
    private ShiftBlacklistRepository shiftBlacklistRepository;


    @GetMapping( {"/", "/index" } )
    public String index(Model model) {
        model.addAttribute("users", userRepository.findAll());
        if( !SecurityContextHolder.getContext().getAuthentication().isAuthenticated() )
        {
            return "redirect:/login";
        }

        User usr  = userRepository.findByUsername(securityService.findLoggedInUsername());
        Optional<Employee> emp = employeeRepository.findEmployeeByUser(usr);

        if(emp.isPresent())
        {
            List<Company> companies = new ArrayList<>();

            for(Map.Entry<Company, CompanyRole> entry : emp.get().getCompanies().entrySet())
                companies.add(entry.getKey());

            model.addAttribute("companies", companies);
        }

        return "index";
    }

    @GetMapping( "/users")
    public String users(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "test";
    }
}

