package dplcreator.controller;

import dplcreator.dpl.entity.Company;
import dplcreator.dpl.entity.CompanyRole;
import dplcreator.dpl.entity.Employee;
import dplcreator.dpl.entity.SelectedCompanyService;
import dplcreator.dpl.repository.CompanyRepository;
import dplcreator.dpl.repository.EmployeeRepository;
import dplcreator.user.User;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@ControllerAdvice
public class GlobalControllerAdvice {

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SelectedCompanyService selectedCompanyService;

    @ModelAttribute("companies")
    public Set<Company> myCompanies() {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Set<Company> out = new HashSet<>();
        if(user instanceof User)
        {

            for(Company cmp :companyRepository.findAll())
            {
                Boolean found = false;
                for(Map.Entry<Employee, CompanyRole> emp : cmp.getEmployees().entrySet())
                {
                    Optional<Employee> employee = employeeRepository.findEmployeeByUser((User) user);

                    if(employee.isPresent() && emp.getKey().getId() == employee.get().getId())
                    {
                        found = true;
                    }
                }
                if(found)
                    out.add(cmp);
            }
        }
        return out;
    }

    @ModelAttribute("mycompany")
    public Company mySelectedCompany() {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Set<Company> out = new HashSet<>();
        if(user instanceof User) {
            var sc = selectedCompanyService.getSelectedCompanyByUser(((User) user));

            if(sc != null)
                return sc.company;
        }
        return null;
    }
}
