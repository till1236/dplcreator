package dplcreator.controller;

import dplcreator.dpl.entity.*;
import dplcreator.dpl.entity.Calendar;
import dplcreator.dpl.repository.*;
import dplcreator.security.SecurityServiceImpl;
import dplcreator.user.User;
import dplcreator.user.UserDetailsServiceImpl;
import dplcreator.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class ShiftController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShiftTemplateRepository shiftTemplateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private SelectedCompanyService selectedCompanyService;

    @Autowired
    private SecurityServiceImpl securityService;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ActualShiftRepository actualShiftRepository;

    @Autowired
    private ShiftBlacklistRepository shiftBlacklistRepository;

    @PostMapping({"/event/setemployee"})
    @ResponseBody
    public ActualShift setShiftEmployee(Model model, @RequestParam(required = true) Long shiftId,
                                        @RequestParam(required = true) Long employeeId)
    {
        Optional<ActualShift> s = actualShiftRepository.findById(shiftId);
        Optional<Employee> user = employeeRepository.findById(employeeId);
        if(!s.isPresent() || !user.isPresent())
        {
            return null;
        }
        s.get().setEmployee(user.get());
        actualShiftRepository.save(s.get());
        return s.get();
    }

    @RequestMapping(value = "/event/add", method = RequestMethod.POST)
    @ResponseBody
    public Shift addEvent(@RequestParam String start1, @RequestParam String end,
                          @RequestParam String date, @RequestParam Long location, @RequestParam(required = false) Optional<String> repeat) {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!(user instanceof User))
        {
            return null;
        }

        SelectedCompany selectedCompany = selectedCompanyService.getSelectedCompanyByUser((User) user);

        if(selectedCompany == null)
            return null;

        Optional<Calendar> cal = calendarRepository.findById(selectedCompany.getCalendar().getId());

        if(cal.isEmpty())
            return null;

        Calendar calendar = cal.get();

        ActualShift as = null;

        String[] ssplit = start1.split(":");
        String[] esplit = end.split(":");

        Time start_time = new Time(Integer.parseInt(ssplit[0]), Integer.parseInt(ssplit[1]), 0);
        Time end_time = new Time(Integer.parseInt(esplit[0]), Integer.parseInt(esplit[1]), 0);

        Date d = new Date(date);
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);

        for(ActualShift shift : calendar.getActualShifts())
        {
            if(shift.getLocation().getId() == location)
            {
                if(shift.getDate() == d && shift.isInTimeRange(start_time, end_time))
                {
                    return null;
                }
            }
        }

        Optional<Location> loc = locationRepository.findById(location);
        Set<Integer> dayofweeks = new HashSet<>();
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(d);
        int dayOfWeek = c.get(java.util.Calendar.DAY_OF_WEEK);
        dayOfWeek = dayOfWeek == 1 ? 6 : dayOfWeek-2;
        if (repeat.isPresent()) {
            String[] repeatdays = repeat.get().split("-");
            Boolean actual = false;
            for (Integer i = 0; i < repeatdays.length; i++) {
                Integer day = Integer.parseInt(repeatdays[i]);
                if (day == 1) {
                    dayofweeks.add(i + 1);

                    if(dayOfWeek == i)
                    {
                        actual = true;
                    }
                }
            }
            ShiftTemplate s = shiftTemplateRepository.save(new ShiftTemplate(start_time,
                    end_time,
                    dayofweeks,
                    loc.isPresent() ? loc.get() : null));

            calendar.addShiftTeplate(s);
            if(actual)
            {
                as = actualShiftRepository.save(new ActualShift(s,d));
                calendar.addActualShift(as);
            }
        } else {
            as = actualShiftRepository.save(new ActualShift(start_time,
                    end_time,
                    d,
                    loc.isPresent() ? loc.get() : null));
            calendar.addActualShift(as);
        }
        calendarRepository.save(calendar);
        return as;
    }

    @PostMapping("/event/delete")
    @ResponseBody
    public String deleteEvent(@RequestParam Long eventId) {
        Optional<ActualShift> loc = actualShiftRepository.findById(eventId);
        if(loc.isPresent()) {

            List<Calendar> calendars = calendarRepository.findAllByActualShifts(loc.get());
            Calendar calendar = calendars.size() == 1 ? calendars.get(0) : null;
            if(calendar != null)
            {
                if(loc.get().getShiftTemplate() != null)
                {
                    ShiftBlacklist sb = new ShiftBlacklist(loc.get());
                    shiftBlacklistRepository.save(sb);
                    calendar.addBlacklist(sb);
                }

                calendar.removeActualShift(loc.get());
                calendar = calendarRepository.save(calendar);
                return "deleted";
            }
            return "not deleted";
        }
        return "not deleted";
    }

    @PostMapping("/event/info")
    @ResponseBody
    public ActualShift getEventInfo(@RequestParam Long eventId) {
        return actualShiftRepository.findById(eventId).get();
    }

    @PostMapping("/event/update")
    @ResponseBody
    public ActualShift updateEvent(@RequestParam Long eventId, @RequestParam String start1, @RequestParam String end,
                                   @RequestParam String date, @RequestParam Long location,@RequestParam Long employeeId, @RequestParam(required = false) Optional<String> repeat) throws ParseException {
        ActualShift shift = actualShiftRepository.findById(eventId).get();

        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!(user instanceof User))
            return null;

        Optional<Employee> emp = employeeRepository.findEmployeeByUser((User)user);
        if(emp.isEmpty())
            return null;

        if(shift != null)
        {
            Optional<Location> loc = locationRepository.findById(location);
            if(loc.isEmpty())
                return null;
            Company companies = companyRepository.findByLocationsContains(loc.get());
            if(companies == null)
            {
                return null;
            }
            if(!companies.getEmployees().containsKey(emp.get()))
                return null;
            for(Calendar cal : companies.getCalendars())
            {
                if(cal.getActualShifts().contains(shift))
                {
                    String[] ssplit = start1.split(":");
                    String[] esplit = end.split(":");

                    Time start_time = new Time(Integer.parseInt(ssplit[0]), Integer.parseInt(ssplit[1]), 0);
                    Time end_time = new Time(Integer.parseInt(esplit[0]), Integer.parseInt(esplit[1]), 0);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = simpleDateFormat.parse(date);

                    if(employeeId == 0) {

                        shift.setEmployee(null);
                    }
                    else
                    {
                        Optional<Employee> employee = employeeRepository.findById(employeeId);
                        if(employee.isEmpty() || !companies.getEmployees().containsKey(employee.get()))
                            return null;
                        shift.setEmployee(employee.get());
                    }
                    shift.setDate(d);
                    shift.setStart(start_time);
                    shift.setEnd(end_time);
                    shift.setLocation(loc.get());
                    shift = actualShiftRepository.save(shift);
                    return shift;
                }
            }
        }
        return null;
    }


}
