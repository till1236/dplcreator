package dplcreator.controller;

import dplcreator.dpl.entity.*;
import dplcreator.dpl.entity.Calendar;
import dplcreator.dpl.repository.*;
import dplcreator.security.SecurityServiceImpl;
import dplcreator.user.User;
import dplcreator.user.UserDetailsServiceImpl;
import dplcreator.user.UserRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class CalendarController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShiftTemplateRepository shiftTemplateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private SelectedCompanyService selectedCompanyService;

    @Autowired
    private SecurityServiceImpl securityService;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ActualShiftRepository actualShiftRepository;

    @Autowired
    private ShiftBlacklistRepository shiftBlacklistRepository;

    public Calendar initDate(Calendar c, Date d)
    {
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(d);
        int dayOfWeek = cal.get(java.util.Calendar.DAY_OF_WEEK);
        dayOfWeek = dayOfWeek == 1 ? 6 : dayOfWeek -2;
        for(ShiftTemplate st : c.getShiftTemplates())
        {
            Boolean skip = false;
            for(ActualShift as : c.getActualShifts())
            {
                if(as.getDate().compareTo(d) == 0 && as.getShiftTemplate() == st)
                {
                    skip = true;
                }
            }
            if(!skip)
            {
                for(ShiftBlacklist sb : c.getBlacklists())
                {
                    ShiftTemplate sst = sb.getBlacklistedShift().getShiftTemplate();
                    if(sst == st && d.compareTo(sb.getBlacklistedShift().getDate()) == 0)
                    {
                        skip = true;
                    }
                }
            }
            if(!skip && st.getDayOfWeeks()[dayOfWeek] == true)
            {
                ActualShift newShift = new ActualShift(st, d);
                newShift = actualShiftRepository.save(newShift);
                c.addActualShift(newShift);

            }
        }
        return calendarRepository.save(c);
    }

    void calculateShiftEmployee(Calendar calendar, Company company, Date date)
    {
        Set<ActualShift> actualShifts = calendar.getActualShifts();
        Set<ActualShift> todaysShifts = new HashSet<>();



        for(ActualShift as : actualShifts)
        {
            if(as.getDate().compareTo(date) == 0)
                todaysShifts.add(as);
        }

        for(ActualShift as : todaysShifts)
        {
            as.setEmployee(null);
        }

        for(ActualShift as : todaysShifts)
        {
            ShiftTemplate st = as.getShiftTemplate();
            if(st != null)
            {
                Employee employee = st.getEmployee();
                Boolean skip = false;
                for(ActualShift as1 : todaysShifts)
                {
                    Employee emp = as1.getEmployee();
                    if(emp == null)
                        continue;
                    if(employee.getId() == emp.getId())
                        skip = true;
                }
                if(skip)
                    continue;;
                if(employee != null)
                    as.setEmployee(employee);
            }
            else
            {
                for(Map.Entry<Employee, CompanyRole> employee_role : company.getEmployees().entrySet())
                {
                    Boolean skip = false;
                    for(ActualShift as1 : todaysShifts)
                    {
                        Employee emp = as1.getEmployee();
                        if(emp == null)
                            continue;
                        if(employee_role.getKey().getId() == emp.getId())
                            skip = true;
                    }
                    if(skip)
                        continue;
                    java.util.Calendar c = java.util.Calendar.getInstance();
                    c.setTime(date);
                    int dayofweek = c.get(java.util.Calendar.DAY_OF_WEEK);
                    dayofweek = dayofweek == 1 ? 6 : dayofweek - 2;
                    Time start = employee_role.getKey().getEmployeeTimes().get(dayofweek).getKey();
                    Time end = employee_role.getKey().getEmployeeTimes().get(dayofweek).getKey();
                    if(as.getStart().after(start) && end.after(as.getEnd()))
                    {
                        as.setEmployee(employee_role.getKey());
                    }
                }
            }
            actualShiftRepository.save(as);
        }

    }


    @GetMapping({"/calendar"})
    public String showCalendar(Model model, @RequestParam(required = false) Optional<Date> date,
                               @RequestParam(required = true) Long CalendarID) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        if (date.isEmpty()) {
            Date mydate = new Date();
            return "redirect:/calendar?date=" + dateFormat.format(mydate) + "&CalendarID=" + CalendarID.toString();
        }
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!(user instanceof User)) {
            return "redirect:/index";
        }

        SelectedCompany selectedCompany = selectedCompanyService.getSelectedCompanyByUser((User) user);
        Optional<Employee> employee = employeeRepository.findEmployeeByUser((User) user);

        if (selectedCompany == null || employee.isEmpty())
            return "redirect:/index";
        Boolean is_emp = false;
        CompanyRole role = null;
        for (Map.Entry<Employee,CompanyRole> emp_role : selectedCompany.company.getEmployees().entrySet())
        {   if (emp_role.getKey().getId() == employee.get().getId()){
                is_emp = true;
                role = emp_role.getValue();
            }
        }
        if(!is_emp)
            return "redirect:/index";


        Optional<Calendar> cal = calendarRepository.findById(CalendarID);

        if(!cal.isPresent() || (model.asMap().get("mycompany") == null))
        {
            return "redirect:/index";
        }
        Calendar calendar = cal.get();
        Company company = selectedCompany.company;
        if(role == CompanyRole.MEMBER)
        {
            Set<ActualShift> actualShifts = calendar.getActualShifts();
            Set<ActualShift> todaysShifts = new HashSet<>();

            for(ActualShift as : actualShifts)
            {
                if(as.getDate().compareTo(date.get()) == 0)
                    todaysShifts.add(as);
            }
            model.addAttribute("events", todaysShifts);
            model.addAttribute("locations", locationRepository.findAllByCompanyOrderById(company));
            java.util.Calendar c = java.util.Calendar.getInstance();
            model.addAttribute("date", dateFormat.format(date.get()));
            c.setTime(date.get());
            c.add(java.util.Calendar.DATE, 1);  // number of days to add
            model.addAttribute("nextdate", dateFormat.format(c.getTime()));
            c.add(java.util.Calendar.DATE, -2);
            model.addAttribute("prevdate", dateFormat.format(c.getTime()));
            model.addAttribute("calendar", calendar);
            return "viewcalendar";
        }

        calendar = initDate(calendar, date.get());

        calculateShiftEmployee(calendar, company, date.get());

        selectedCompany.setCalendar(calendar);

        Set<ActualShift> actualShifts = calendar.getActualShifts();
        Set<ActualShift> todaysShifts = new HashSet<>();

        for(ActualShift as : actualShifts)
        {
            if(as.getDate().compareTo(date.get()) == 0)
                todaysShifts.add(as);
        }

        java.util.Calendar c = java.util.Calendar.getInstance();
        model.addAttribute("events", todaysShifts);
        model.addAttribute("locations", locationRepository.findAllByCompanyOrderById(company));

        model.addAttribute("date", dateFormat.format(date.get()));
        c.setTime(date.get());
        c.add(java.util.Calendar.DATE, 1);  // number of days to add
        model.addAttribute("nextdate", dateFormat.format(c.getTime()));
        c.add(java.util.Calendar.DATE, -2);
        model.addAttribute("prevdate", dateFormat.format(c.getTime()));
        model.addAttribute("calendar", calendar);


        return "calendar";
    }

    @GetMapping("/calendar/add")
    public String addCalendar(Model model)
    {
        return "calendar_add";
    }

    @PostMapping("/calendar/add")
    public String addCalendar(Model model, @RequestParam String calendarName)
    {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        SelectedCompany selectedCompany = selectedCompanyService.getSelectedCompanyByUser(user);
        if(selectedCompany != null)
        {
            Calendar calendar = new Calendar(calendarName);
            calendar = calendarRepository.save(calendar);
            Company company = selectedCompany.company;
            company.addCalendar(calendar);
            company = companyRepository.save(company);
        }
        return "redirect:/index";
    }
}
