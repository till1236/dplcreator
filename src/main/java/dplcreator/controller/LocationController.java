package dplcreator.controller;

import dplcreator.dpl.entity.*;
import dplcreator.dpl.repository.*;
import dplcreator.security.SecurityServiceImpl;
import dplcreator.user.User;
import dplcreator.user.UserDetailsServiceImpl;
import dplcreator.user.UserRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class LocationController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShiftTemplateRepository shiftTemplateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private SelectedCompanyService selectedCompanyService;

    @Autowired
    private SecurityServiceImpl securityService;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ActualShiftRepository actualShiftRepository;

    @Autowired
    private ShiftBlacklistRepository shiftBlacklistRepository;

    @PostMapping("/location/add")
    @ResponseBody
    public Location addLocation(@RequestParam String name) {
        Location l = new Location(name);
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!(user instanceof User))
            return null;
        SelectedCompany sc = selectedCompanyService.getSelectedCompanyByUser((User) user);
        if(sc == null)
            return null;

        Optional<Company> cmp = companyRepository.findById(sc.company.getId());
        if(!cmp.isPresent())
            return null;
        Company company = cmp.get();
        l.setCompany(company);
        l = locationRepository.save(l);
        company.addLocation(l);
        companyRepository.save(company);
        return l;
    }

    @PostMapping("/employee/location/remove")
    @ResponseBody
    public Boolean removeLocationToEmployee(Model model, @RequestParam String locationids)
    {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!(user instanceof User) || locationids == "")
            return null;

        Optional<Employee> emp = employeeRepository.findEmployeeByUser((User) user);


        if(emp.isEmpty())
            return false;

        Employee employee = emp.get();

        List<String> splitted = Arrays.asList(locationids.split("-"));

        if(splitted.size() == 0)
            return false;

        for(String s : splitted)
        {
            Long locationid = Long.parseLong(s);
            Optional<Location> loc = locationRepository.findById(locationid);
            if(loc.isEmpty())
                continue;
            Location location = loc.get();

            employee.removeLocationById(locationid);
        }
        employeeRepository.save(employee);
        return true;
    }

    @PostMapping("/employee/location/add")
    @ResponseBody
    public Boolean addLocationToEmployee(Model model, @RequestParam String locationids)
    {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!(user instanceof User) || locationids == "")
            return null;

        Optional<Employee> emp = employeeRepository.findEmployeeByUser((User) user);


        if(emp.isEmpty())
            return false;

        Employee employee = emp.get();

        List<String> splitted = Arrays.asList(locationids.split("-"));

        if(splitted.size() == 0)
            return false;

        for(String s : splitted)
        {
            Long locationid = Long.parseLong(s);
            Location location = locationRepository.getOne(locationid);
            if(location == null)
                continue;

            if(employee.getLocations().contains(location))
                continue;

            Boolean addable = false;
            for(Map.Entry<Company, CompanyRole> c : employee.getCompanies().entrySet())
            {
                for(Location loc : c.getKey().getLocations())
                {
                    if(loc.getId() == locationid)
                        addable = true;
                }
            }
            if(addable)
            {
                employee.addLocation(location);
            }
        }
        employeeRepository.save(employee);
        return true;
    }
}
