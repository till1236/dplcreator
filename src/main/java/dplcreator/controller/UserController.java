package dplcreator.controllers;

import dplcreator.dpl.entity.Employee;
import dplcreator.dpl.repository.EmployeeRepository;
import dplcreator.security.SecurityService;
import dplcreator.security.SecurityServiceImpl;
import dplcreator.user.*;
import org.apache.tomcat.util.buf.B2CConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityServiceImpl securityService;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/registration")
    public String registration(Model model) {
        if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() != "anonymousUser")
            return "redirect:/index";
        model.addAttribute("userForm", new UserForm());

        return "registration";
    }

    @GetMapping("/profile")
    public String profile(Model model, @RequestParam Optional<Long> employeeId) {
        if(employeeId.isEmpty()) {
            Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (!(user instanceof User))
                return "redirect:/index";

            Optional<Employee> emp = employeeRepository.findEmployeeByUser((User) user);

            if (emp.isEmpty())
                return "redirect:/index";

            model.addAttribute("logged_in_user", (User) user);
            model.addAttribute("current_employee", emp.get());
        }
        else
        {
            Optional<Employee> emp = employeeRepository.findById(employeeId.get());

            if(emp.isEmpty())
                return "redirect:/index";

            model.addAttribute("current_employee", emp.get());
        }

        return "user";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserForm userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        User user = userService.save(new User(userForm.getUsername(), userForm.getPassword(), (ArrayList<Role>) roleRepository.findAll()));
        Employee employee = userForm.toEmployee();
        employee.setUser(user);
        employeeRepository.save(employee);


        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/index";
    }

    @GetMapping(value = "/login")
    public String showLogin(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        model.addAttribute("error1", error);
        return "login";
    }

    @GetMapping(value = "/profile/edit")
    public String editProfile(Model model) {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!(user instanceof User))
            return "redirect:/index";

        Optional<Employee> emp = employeeRepository.findEmployeeByUser((User) user);

        if (emp.isEmpty())
            return "redirect:/index";

        model.addAttribute("logged_in_user", (User) user);
        model.addAttribute("current_employee", emp.get());

        return "editprofile";
    }

    @PostMapping(value = "/profile/edit")
    public String editProfile(Model model, @RequestParam String username, @RequestParam String email,
                              @RequestParam String prename, @RequestParam String lastname,
                                @RequestParam String mo_from, @RequestParam String mo_to,
                              @RequestParam String tu_from, @RequestParam String tu_to,
                              @RequestParam String we_from, @RequestParam String we_to,
                              @RequestParam String th_from, @RequestParam String th_to,
                              @RequestParam String fr_from, @RequestParam String fr_to,
                              @RequestParam String sa_from, @RequestParam String sa_to,
                              @RequestParam String so_from, @RequestParam String so_to) {


        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!(user instanceof User))
            return "redirect:/index";

        Optional<Employee> emp = employeeRepository.findEmployeeByUser((User) user);

        if (emp.isEmpty())
            return "redirect:/index";

        Employee employee = emp.get();
        employee.setEmail(email);
        employee.setLastname(lastname);
        employee.setPrename(prename);
        employee.setEmployeeTime(0,
                new Time(Integer.parseInt(mo_from.split(":")[0]),Integer.parseInt(mo_from.split(":")[1]), 0),
                new Time(Integer.parseInt(mo_to.split(":")[0]),Integer.parseInt(mo_to.split(":")[1]), 0));
        employee.setEmployeeTime(1,
                new Time(Integer.parseInt(tu_from.split(":")[0]),Integer.parseInt(tu_from.split(":")[1]), 0),
                new Time(Integer.parseInt(tu_to.split(":")[0]),Integer.parseInt(tu_to.split(":")[1]), 0));
        employee.setEmployeeTime(2,
                new Time(Integer.parseInt(we_from.split(":")[0]),Integer.parseInt(we_from.split(":")[1]), 0),
                new Time(Integer.parseInt(we_to.split(":")[0]),Integer.parseInt(we_to.split(":")[1]), 0));
        employee.setEmployeeTime(3,
                new Time(Integer.parseInt(th_from.split(":")[0]),Integer.parseInt(th_from.split(":")[1]), 0),
                new Time(Integer.parseInt(th_to.split(":")[0]),Integer.parseInt(th_to.split(":")[1]), 0));
        employee.setEmployeeTime(4,
                new Time(Integer.parseInt(fr_from.split(":")[0]),Integer.parseInt(fr_from.split(":")[1]), 0),
                new Time(Integer.parseInt(fr_to.split(":")[0]),Integer.parseInt(fr_to.split(":")[1]), 0));
        employee.setEmployeeTime(5,
                new Time(Integer.parseInt(sa_from.split(":")[0]),Integer.parseInt(sa_from.split(":")[1]), 0),
                new Time(Integer.parseInt(sa_to.split(":")[0]),Integer.parseInt(sa_to.split(":")[1]), 0));
        employee.setEmployeeTime(6,
                new Time(Integer.parseInt(so_from.split(":")[0]),Integer.parseInt(so_from.split(":")[1]), 0),
                new Time(Integer.parseInt(so_to.split(":")[0]),Integer.parseInt(so_to.split(":")[1]), 0));


        employeeRepository.save(employee);
        
        return "redirect:/profile";
    }

}