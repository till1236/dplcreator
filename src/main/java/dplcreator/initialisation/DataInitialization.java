package dplcreator.initialisation;

import dplcreator.dpl.entity.*;
import dplcreator.dpl.repository.*;
import dplcreator.user.RoleRepository;
import dplcreator.user.User;
import dplcreator.user.Role;
import dplcreator.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


@Component
public class DataInitialization implements ApplicationRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private ShiftTemplateRepository shiftTemplateRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CalendarRepository calendarRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    public DataInitialization() {
    }

    public void run(ApplicationArguments args) {
        ArrayList<Role> roles = new ArrayList<Role>();
        Role role = new Role("ADMIN");
        role = roleRepository.save(role);
        roles.add(role);
        role = new Role("MODERATOR");
        role = roleRepository.save(role);
        roles.add(role);
        role = new Role("USER");
        role = roleRepository.save(role);
        roles.add(role);
        role = new Role("GUEST");
        role = roleRepository.save(role);
        roles.add(role);

        User user1 = new User("sebbedermaster1", bCryptPasswordEncoder.encode("sebbedermaster1"), roles);
        user1 = userRepository.save(user1);
        Company comp = companyRepository.save(new Company("Cafe Nepomuk"));
        User user2 = userRepository.save( new User("sebbedermaster2", bCryptPasswordEncoder.encode("sebbedermaster"), roles));
        User user3 = userRepository.save(new User("sebbedermaster3", bCryptPasswordEncoder.encode("sebbedermaster"), roles));
        User user4 = userRepository.save(new User("sebbedermaster4", bCryptPasswordEncoder.encode("sebbedermaster"), roles));
        Employee emp1 = employeeRepository.save(new Employee( "Tutt", "Brand", ""));
        Employee emp2 = employeeRepository.save(new Employee( "Chrissi", "", ""));
        Employee emp3 = employeeRepository.save(new Employee( "Till", "Brand",""));
        Employee emp4 = employeeRepository.save(new Employee( "Hugo", "Hotte",""));
        emp1.setUser(user1);emp2.setUser(user2);emp3.setUser(user3);emp4.setUser(user4);
        emp1 = employeeRepository.save(emp1);emp2 = employeeRepository.save(emp2);emp3 = employeeRepository.save(emp3);emp4 = employeeRepository.save(emp4);
        comp.addEmployee(emp1, CompanyRole.ADMIN);comp.addEmployee(emp2, CompanyRole.MODERATOR);comp.addEmployee(emp3, CompanyRole.MEMBER);comp.addEmployee(emp4, CompanyRole.MEMBER);
        emp1 = employeeRepository.save(emp1);
        emp2 = employeeRepository.save(emp2);
        emp3 = employeeRepository.save(emp3);
        emp4 = employeeRepository.save(emp4);


        Location theke=locationRepository.save(new Location("Theke"));
        theke.setCompany(comp);
        theke = locationRepository.save(theke);
        Location service = locationRepository.save(new Location("Service"));
        service.setCompany(comp);
        service = locationRepository.save(service);
        Location service_zusatz = locationRepository.save(new Location("Service Zusatz"));
        service_zusatz.setCompany(comp);
        service_zusatz = locationRepository.save(service_zusatz);
        Location essenstraeger= locationRepository.save(new Location("Essensträger"));
        essenstraeger.setCompany(comp);
        essenstraeger = locationRepository.save(essenstraeger);
        comp.addLocation(theke);
        comp.addLocation(service);
        comp.addLocation(service_zusatz);
        comp.addLocation(essenstraeger);
        Set<Integer> dayofweeks = new HashSet<Integer>();
        dayofweeks.add(1);dayofweeks.add(3);dayofweeks.add(4);dayofweeks.add(6);dayofweeks.add(7);
        ShiftTemplate s1 = new ShiftTemplate(new Time(10, 0, 0),
                new Time(14, 30, 0),
                dayofweeks, theke);
        ShiftTemplate s2 = new ShiftTemplate(new Time(17, 30, 0),
                new Time(0, 30, 0),
                dayofweeks, theke);
        s1.setEmployee(emp1);s2.setEmployee(emp2);

        s1 = shiftTemplateRepository.save(s1);
        s2 = shiftTemplateRepository.save(s2);

        ShiftTemplate s3 = new ShiftTemplate(new Time(11, 0, 0),
                new Time(17, 30, 0),
                dayofweeks, service);
        ShiftTemplate s4 =new ShiftTemplate(new Time(17, 30, 0),
                new Time(23, 0, 0),
                dayofweeks, service);
        s3.setEmployee(emp3);s4.setEmployee(emp4);
        user3 = userRepository.save(user3);
        user4 = userRepository.save(user4);

        s3 = shiftTemplateRepository.save(s3);
        s4 = shiftTemplateRepository.save(s4);

        ShiftTemplate s5 =new ShiftTemplate(new Time(12, 0, 0),
                new Time(14, 00, 0),
                dayofweeks, service_zusatz);
        ShiftTemplate s6 =new ShiftTemplate(new Time(18, 0, 0),
                        new Time(21, 0, 0),
                        dayofweeks, service_zusatz);
        s5.setEmployee(emp1);s6.setEmployee(emp2);
        s5 = shiftTemplateRepository.save(s5);
        s6 = shiftTemplateRepository.save(s6);

        ShiftTemplate s7 =new ShiftTemplate(new Time(12, 0, 0),
                new Time(14, 00, 0),
                dayofweeks, essenstraeger);
        ShiftTemplate s8 =new ShiftTemplate(new Time(18, 0, 0),
                        new Time(21, 0, 0),
                        dayofweeks, essenstraeger);
        s7.setEmployee(emp3);s8.setEmployee(emp4);
        s7 = shiftTemplateRepository.save(s7);
        s8 = shiftTemplateRepository.save(s8);



        Calendar calendar = calendarRepository.save(new Calendar("First Calendar"));
        calendar.addShiftTeplate(s1);calendar.addShiftTeplate(s2);calendar.addShiftTeplate(s3);calendar.addShiftTeplate(s4);
        calendar.addShiftTeplate(s5);calendar.addShiftTeplate(s6);calendar.addShiftTeplate(s7);calendar.addShiftTeplate(s8);

        calendar = calendarRepository.save(calendar);
        comp.addCalendar(calendar);

        companyRepository.save(comp);

        comp = companyRepository.save(new Company("Rebstöckle"));

        comp.addEmployee(emp1, CompanyRole.ADMIN);comp.addEmployee(emp2, CompanyRole.MODERATOR);
        comp.addEmployee(emp3, CompanyRole.MEMBER);comp.addEmployee(emp4, CompanyRole.MEMBER);
        emp1 = employeeRepository.save(emp1);
        emp2 = employeeRepository.save(emp2);
        emp3 = employeeRepository.save(emp3);
        emp4 = employeeRepository.save(emp4);

         theke=locationRepository.save(new Location("Küche 1"));
         theke.setCompany(comp);
         theke = locationRepository.save(theke);
         service = locationRepository.save(new Location("Küche 2"));
        service.setCompany(comp);
        service = locationRepository.save(service);
         service_zusatz = locationRepository.save(new Location("Keks Zusatz"));
        service_zusatz.setCompany(comp);
        service_zusatz = locationRepository.save(service_zusatz);
         essenstraeger= locationRepository.save(new Location("Saladette"));
        essenstraeger.setCompany(comp);
        essenstraeger = locationRepository.save(essenstraeger);

        comp.addLocation(theke);
        comp.addLocation(service);
        comp.addLocation(service_zusatz);
        comp.addLocation(essenstraeger);

        s1 = new ShiftTemplate(new Time(10, 0, 0),
                new Time(14, 30, 0),
                dayofweeks, theke);
        s2 = new ShiftTemplate(new Time(17, 30, 0),
                new Time(0, 30, 0),
                dayofweeks, theke);
        s1.setEmployee(emp1);s2.setEmployee(emp2);
        s1 = shiftTemplateRepository.save(s1);
        s2 = shiftTemplateRepository.save(s2);

        s3 = new ShiftTemplate(new Time(11, 0, 0),
                new Time(17, 30, 0),
                dayofweeks, service);
        s4 =new ShiftTemplate(new Time(17, 30, 0),
                new Time(23, 0, 0),
                dayofweeks, service);
        s3.setEmployee(emp3);s4.setEmployee(emp4);
        s3 = shiftTemplateRepository.save(s3);
        s4 = shiftTemplateRepository.save(s4);

        s5 =new ShiftTemplate(new Time(12, 0, 0),
                new Time(14, 00, 0),
                dayofweeks, service_zusatz);
        s6 =new ShiftTemplate(new Time(18, 0, 0),
                new Time(21, 0, 0),
                dayofweeks, service_zusatz);
        s5.setEmployee(emp1);s6.setEmployee(emp2);
        s5 = shiftTemplateRepository.save(s5);
        s6 = shiftTemplateRepository.save(s6);

        s7 =new ShiftTemplate(new Time(12, 0, 0),
                new Time(14, 00, 0),
                dayofweeks, essenstraeger);
        s8 =new ShiftTemplate(new Time(18, 0, 0),
                new Time(21, 0, 0),
                dayofweeks, essenstraeger);
        s7.setEmployee(emp3);s8.setEmployee(emp4);
        s7 = shiftTemplateRepository.save(s7);
        s8 = shiftTemplateRepository.save(s8);



        calendar = calendarRepository.save(new Calendar("Second Calendar"));
        calendar.addShiftTeplate(s1);calendar.addShiftTeplate(s2);calendar.addShiftTeplate(s3);calendar.addShiftTeplate(s4);
        calendar.addShiftTeplate(s5);calendar.addShiftTeplate(s6);calendar.addShiftTeplate(s7);calendar.addShiftTeplate(s8);

        calendar = calendarRepository.save(calendar);
        comp.addCalendar(calendar);

        companyRepository.save(comp);

        comp = companyRepository.save(new Company("Lonely Company"));
    }
}
