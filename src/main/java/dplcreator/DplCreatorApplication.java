package dplcreator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DplCreatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(DplCreatorApplication.class, args);
	}

}
