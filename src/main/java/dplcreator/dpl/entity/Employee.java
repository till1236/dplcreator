package dplcreator.dpl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dplcreator.user.User;
import javafx.util.Pair;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.persistence.*;
import java.sql.Time;
import java.util.*;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JsonIgnore
    private User user;

    @ElementCollection
    @MapKeyColumn(name="companie_roles")
    @Column(name="roles")
    @CollectionTable(name="companies", joinColumns=@JoinColumn(name="employee_id"))
    @JsonIgnore
    private Map<Company, CompanyRole> companies;

    private String lastname;
    private String prename;
    @JsonIgnore
    private String email;


    @ElementCollection
    @MapKeyColumn(name="dayofweek")
    @Column(name="times")
    @CollectionTable(name="employee_times", joinColumns=@JoinColumn(name="employee_id"))
    @JsonIgnore
    private Map<Integer, Pair<Time, Time>> employeeTimes;

    @OneToMany
    @JsonIgnore
    private List<VocationApplication> vocationApplications;

    @OneToMany
    @JsonIgnore
    private List<Location> locationList;

    public void initEmployeeTimes()
    {
        employeeTimes.put(0, new Pair<Time,Time>(new Time(0), new Time(0)));
        employeeTimes.put(1, new Pair<Time,Time>(new Time(0), new Time(0)));
        employeeTimes.put(2, new Pair<Time,Time>(new Time(0), new Time(0)));
        employeeTimes.put(3, new Pair<Time,Time>(new Time(0), new Time(0)));
        employeeTimes.put(4, new Pair<Time,Time>(new Time(0), new Time(0)));
        employeeTimes.put(5, new Pair<Time,Time>(new Time(0), new Time(0)));
        employeeTimes.put(6, new Pair<Time,Time>(new Time(0), new Time(0)));
    }
    public Employee(String _prename, String _lastname, String email) {
        companies = new HashMap<>();
        this.lastname = _lastname;
        this.prename = _prename;
        this.email = email;
        employeeTimes = new HashMap<>();
        initEmployeeTimes();
        vocationApplications = new ArrayList<>();
        locationList = new ArrayList<>();
    }
    public Employee(){locationList = new ArrayList<>();vocationApplications = new ArrayList<>();companies = new HashMap<>();employeeTimes = new HashMap<>();initEmployeeTimes();}

    public List<Location> getLocations() {
        return locationList;
    }

    public void setLocations(List<Location> locationList) {
        this.locationList = locationList;
    }

    public void addLocation(Location loc)
    {
        this.locationList.add(loc);
    }

    public Long getId() {
        return id;
    }

    public void setEmployeeTime(int dayofweek, Time from, Time to)
    {
        if(dayofweek < 1 || dayofweek > 7)
            return;
        employeeTimes.put(dayofweek, new Pair(from, to));
    }

    public Map<Integer, Pair<Time,Time>> getEmployeeTimes() {
        return employeeTimes;
    }

    public void setEmployeeTimes(Map<Integer, Pair<Time,Time>> employeeTimes) {
        this.employeeTimes = employeeTimes;
    }

    public List<VocationApplication> getVocationApplications() {
        return vocationApplications;
    }

    public void setVocationApplications(List<VocationApplication> vocationApplications) {
        this.vocationApplications = vocationApplications;
    }

    public void addVocationApplication(VocationApplication vc)
    {
        this.vocationApplications.add(vc);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPrename() {
        return prename;
    }

    public void setPrename(String prename) {
        this.prename = prename;
    }

    public Map<Company,CompanyRole> getCompanies() {
        return companies;
    }
    public void addCompany(Company r, CompanyRole role){companies.put(r,role);}

    public void setCompanies(Map<Company, CompanyRole> companies) {
        this.companies = companies;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void removeLocationById(Long id)
    {
        for(Integer i = 0; i < getLocations().size(); i++)
        {
            if(id == getLocations().get(i).getId())
            {
                locationList.remove(getLocations().get(i));
                return;
            }
        }
    }

    public CompanyRole getRoleByCompany(Company e){
        for (Map.Entry<Company, CompanyRole> p : companies.entrySet()
        ) {
            if(p.getKey().getId() == e.getId())
                return p.getValue();
        }
        return null;
    }


}
