package dplcreator.dpl.entity;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
public class ActualShift extends Shift {

    protected Date date;


    @ManyToOne
    private ShiftTemplate shiftTemplate;

    public ActualShift(Time start, Time end, Date date, Location shiftlocation) {
       super(start,end,shiftlocation);
        this.date = date;
    }
    public ActualShift(ShiftTemplate shiftTemplate, Date date)
    {
        super(shiftTemplate.getStart(), shiftTemplate.getEnd(), shiftTemplate.getLocation());
        this.employee = shiftTemplate.getEmployee();
        this.shiftTemplate = shiftTemplate;
        this.date = date;
    }

    public ActualShift(){super();}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ShiftTemplate getShiftTemplate() {
        return shiftTemplate;
    }

    public void setShiftTemplate(ShiftTemplate shiftTemplate) {
        this.shiftTemplate = shiftTemplate;
    }
}
