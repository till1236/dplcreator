package dplcreator.dpl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.*;

@Entity
public class Calendar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany
    @JsonIgnore
    private Set<ShiftTemplate> shiftTemplates;

    @OneToMany
    @JsonIgnore
    private Set<ActualShift> actualShifts;

    @OneToMany
    @JsonIgnore
    private Set<ShiftBlacklist> blacklists;

    public Calendar(String _name)
    {
        name = _name;
        shiftTemplates = new HashSet<ShiftTemplate>();
        actualShifts = new HashSet<ActualShift>();
        blacklists = new HashSet<>();
    }

    public Calendar(){
        shiftTemplates = new HashSet<ShiftTemplate>();
        actualShifts = new HashSet<ActualShift>();
        blacklists = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addShiftTeplate(ShiftTemplate c)
    {
        shiftTemplates.add(c);
    }

    public Set<ShiftTemplate> getShiftTemplates(){return shiftTemplates;}

    public void setShiftTemplates(Set<ShiftTemplate> shifts) {
        this.shiftTemplates = shifts;
    }

    public Set<ActualShift> getActualShifts() {
        return actualShifts;
    }

    public void removeActualShift(ActualShift as)
    {
        if(actualShifts.contains(as))
            actualShifts.remove(as);
    }

    public void addActualShift(ActualShift c)
    {
        actualShifts.add(c);
    }

    public void setActualShifts(Set<ActualShift> actualShifts) {
        this.actualShifts = actualShifts;
    }

    public void addBlacklist(ShiftBlacklist ac)
    {
        if(blacklists.contains(ac))
            return;

        blacklists.add(ac);
    }

    public void setBlacklists(Set<ShiftBlacklist> blacklists) {
        this.blacklists = blacklists;
    }

    public Set<ShiftBlacklist> getBlacklists()
    {
        return blacklists;
    }

    public Set<ActualShift> buildActualShiftsFromShiftTemplateForDate(Date date)
    {
        Set<ActualShift> out = new HashSet<ActualShift>();

        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(date);
        int dayOfWeek = c.get(java.util.Calendar.DAY_OF_WEEK);
        dayOfWeek = dayOfWeek == 1 ? 6 : dayOfWeek-2;
        for(ShiftTemplate shiftTemplate : shiftTemplates)
        {
            if(!shiftTemplate.getDayOfWeeks()[dayOfWeek])
            {
                continue;
            }
            Boolean skip = false;
            for(ShiftBlacklist shiftBlacklist : blacklists)
            {
                ShiftTemplate st = shiftBlacklist.getBlacklistedShift().getShiftTemplate();
                if(st != null)
                {
                    if(st == shiftTemplate)
                        skip = true;
                }
            }
            if(skip)
                continue;
            ActualShift actualShift = new ActualShift(shiftTemplate, date);
            out.add(actualShift);
        }
        return out;
    }
}
