package dplcreator.dpl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dplcreator.user.User;
import javafx.util.Pair;

import javax.persistence.*;
import java.util.*;

@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @OneToMany
    private Set<Calendar> calendars;

    @ElementCollection
    @MapKeyColumn(name="employee_roles")
    @Column(name="roles")
    @CollectionTable(name="employees", joinColumns=@JoinColumn(name="company_id"))
    @JsonIgnore
    private Map<Employee, CompanyRole> employees;

    @OneToMany
    private Set<Location> locations;

    @OneToMany
    private Set<Employee> applications;

    public Company(String _name)
    {
        employees = new HashMap<>();
        locations = new HashSet<Location>();
        calendars = new HashSet<Calendar>();
        applications = new HashSet<Employee>();
        name = _name;
    }

    public Company(){
        employees = new HashMap<>();
        locations = new HashSet<Location>();
        calendars = new HashSet<Calendar>();
        applications = new HashSet<Employee>();
    }

    public void addEmployee(Employee c, CompanyRole role)
    {
        employees.put(c,role);
        c.addCompany(this, role);
    }

    public CompanyRole getRoleByEmployee(Employee e){
        for (Map.Entry<Employee, CompanyRole> entry : employees.entrySet()) {
            if (entry.getKey().getId() == e.getId())
                return entry.getValue();
        }
        return null;
    }

    public Map<Employee, CompanyRole> getEmployees(){return employees;}

    public void addLocation(Location c)
    {
        locations.add(c);
    }

    public Set<Location> getLocations(){return locations;}
    public void setLocations(Set<Location> _locations) {locations = _locations;}

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addCalendar(Calendar c)
    {
        calendars.add(c);
    }

    public Set<Calendar> getCalendars(){return calendars;}

    public void setCalendars(Set<Calendar> calendars) {
        this.calendars = calendars;
    }

    public void setEmployees(Map<Employee, CompanyRole> employees) {
        this.employees = employees;
    }

    public Set<Employee> getApplications() {
        return applications;
    }

    public void setApplications(Set<Employee> applications) {
        this.applications = applications;
    }

    public Boolean hasEmployee(Employee employee)
    {
        for(Map.Entry<Employee, CompanyRole> pair : employees.entrySet())
            if(pair.getKey().getId() == employee.getId())
                return true;
        return false;
    }


}
