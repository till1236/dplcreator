package dplcreator.dpl.entity;

import dplcreator.user.User;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SelectedCompanyService {

    Set<SelectedCompany> selectedCompanies;
    public SelectedCompanyService()
    {
        selectedCompanies=new HashSet<>();
    }

    public SelectedCompany getSelectedCompanyByUser(User user)
    {
        for(SelectedCompany sc : selectedCompanies)
        {
            if(sc.user.getId() == user.getId())
            {
                return sc;
            }
        }
        return null;
    }

    public void addSelectedCompany(SelectedCompany sc)
    {
        if(!SelectedCompaniesContains(sc.user))
        {
            selectedCompanies.add(sc);
            return;
        }

        for(SelectedCompany sc1 : selectedCompanies)
        {
            if(sc1.user.getId() == sc.user.getId())
            {
                sc.company = sc1.company;
            }
        }
    }

    public void removeSelectedCompany(SelectedCompany sc)
    {
        selectedCompanies.remove(sc);
    }

    public Boolean SelectedCompaniesContains(User user)
    {
        for(SelectedCompany sc : selectedCompanies)
        {
            if(sc.user.getId() == user.getId())
            {
                return true;
            }
        }
        return false;
    }
}
