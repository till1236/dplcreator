package dplcreator.dpl.entity;

import javax.persistence.Entity;

public enum CompanyRole
{
    MEMBER,
    MODERATOR,
    ADMIN
}
