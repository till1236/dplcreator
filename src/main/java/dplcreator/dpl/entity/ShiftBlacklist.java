package dplcreator.dpl.entity;

import javax.persistence.*;

@Entity
public class ShiftBlacklist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private ActualShift blacklistedShift;

    public ShiftBlacklist(ActualShift ac)
    {
        blacklistedShift = ac;
    }

    public ShiftBlacklist(){}

    public Long getId() {
        return id;
    }

    public ActualShift getBlacklistedShift() {
        return blacklistedShift;
    }

    public void setBlacklistedShift(ActualShift blacklistedShift) {
        this.blacklistedShift = blacklistedShift;
    }
}
