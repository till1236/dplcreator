package dplcreator.dpl.entity;

import javax.persistence.*;
import java.sql.Time;

@Entity
public abstract class Shift {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    public Long getId(){return id;}

    protected Time start;
    protected Time end;


    @ManyToOne
    protected Employee employee;

    @ManyToOne
    protected Location location;

    public Shift(Time start, Time end, Location location) {
        this.start = start;
        this.end = end;
        this.location = location;
    }

    public Shift() {}

    public Time getStart() {
        return start;
    }

    public void setStart(Time start) {
        this.start = start;
    }

    public Time getEnd() {
        return end;
    }

    public void setEnd(Time end) {
        this.end = end;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Boolean isInTimeRange(Time _start, Time _end)
    {
        return (start.after(_start) && end.before(_start)) || (start.after(_end) && end.before(_end)) ;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
