package dplcreator.dpl.entity;

import dplcreator.user.User;

public class SelectedCompany {

    public User user;
    public Company company;
    public Calendar calendar;

    public SelectedCompany(User _user, Company _company)
    {
        user = _user;
        company = _company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }
}
