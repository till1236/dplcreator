package dplcreator.dpl.entity;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.sql.Time;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ShiftTemplate extends Shift{

    protected Boolean[] dayOfWeeks;

    public ShiftTemplate(Time s, Time e, Set<Integer> _dayOfWeeks, Location location)
    {
        super(s,e,location);
        dayOfWeeks =new Boolean[]{false,false,false,false,false,false,false};
        for(Integer day : _dayOfWeeks)
        {
            dayOfWeeks[day-1] = true;
        }
        this.dayOfWeeks = dayOfWeeks;
    }

    public ShiftTemplate()
    {
        super();
        dayOfWeeks=new Boolean[]{false,false,false,false,false,false,false};
    }

    public Set<ActualShift> getShiftsInRange(Date start, Date end)
    {
        Set<ActualShift> shifts = new HashSet<>();
        Set<Boolean> _dayofweeks = new HashSet<>(Arrays.asList(dayOfWeeks));
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(start);
        while(c.getTime().getTime() <= end.getTime())
        {
            int dayOfWeek = c.get(java.util.Calendar.DAY_OF_WEEK);
            if(dayOfWeeks[dayOfWeek-1] == true)
            {
                shifts.add(new ActualShift(this.start, this.end, start,location));
            }
            c.add(Calendar.DATE, 1);
        }
        return shifts;
    }

    public Time getStart() {
        return start;
    }

    public void setStart(Time start) {
        this.start = start;
    }

    public Time getEnd() {
        return end;
    }

    public void setEnd(Time end) {
        this.end = end;
    }



    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Boolean[] getDayOfWeeks() {
        return dayOfWeeks;
    }

    public void setDayOfWeeks(Boolean[] dayOfWeeks) {
        this.dayOfWeeks = dayOfWeeks;
    }
}
