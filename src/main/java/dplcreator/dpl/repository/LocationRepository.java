package dplcreator.dpl.repository;

import dplcreator.dpl.entity.Company;
import dplcreator.dpl.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

    Optional<Location> findFirstByName(String name);
    List<Location> findAllByCompanyOrderById(Company company);
}
