package dplcreator.dpl.repository;

import dplcreator.dpl.entity.Employee;
import dplcreator.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findEmployeeByUser(User user);

}
