package dplcreator.dpl.repository;

import dplcreator.dpl.entity.VocationApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VocationApplicationRepository extends JpaRepository<VocationApplication, Long> {
}
