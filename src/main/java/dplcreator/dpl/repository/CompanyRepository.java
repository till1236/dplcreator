package dplcreator.dpl.repository;

import dplcreator.dpl.entity.*;
import dplcreator.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    Optional<Company> findFirstByName(String name);
    Optional<Company> findByCalendarsContains(Calendar calendar);
    Company findByLocationsContains(Location loc);
    List<Company> findAllByNameIsContaining(String s);

    List<Company> findAllByNameIsContainingAndEmployeesNotContaining(String s, Employee employee);
}
