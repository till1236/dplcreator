package dplcreator.dpl.repository;

import dplcreator.dpl.entity.ActualShift;
import dplcreator.dpl.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ActualShiftRepository extends JpaRepository<ActualShift, Long> {

    List<ActualShift> findAllByLocation(Location location);
    List<ActualShift> findAllByDate(Date date);
}
