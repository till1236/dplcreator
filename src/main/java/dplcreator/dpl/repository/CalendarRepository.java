package dplcreator.dpl.repository;

import dplcreator.dpl.entity.ActualShift;
import dplcreator.dpl.entity.Calendar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CalendarRepository extends JpaRepository<Calendar, Long> {
    List<Calendar> findAllByActualShifts(ActualShift actualShift);
}
