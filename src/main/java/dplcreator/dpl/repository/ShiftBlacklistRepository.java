package dplcreator.dpl.repository;

import dplcreator.dpl.entity.ShiftBlacklist;
import dplcreator.dpl.entity.ShiftTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ShiftBlacklistRepository extends JpaRepository<ShiftBlacklist, Long> {

    List<ShiftBlacklist> findAllByBlacklistedShift_DateAndBlacklistedShift_ShiftTemplate(Date date, ShiftTemplate shiftTemplate);
    List<ShiftBlacklist> findAllByBlacklistedShift_Date(Date date);
}
