package dplcreator.user;

public interface UserService {
    User save(User user);


    User findByUsername(String username);
}
