package dplcreator.user;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    public List<User> getUsers(){return users;}
    public void setUsers(ArrayList<User> us){users = us;}
    public void addUser(User u){users.add(u);}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role(String _name)
    {
        setName(_name);
        users = new ArrayList<User>();
    }

    public Role(){users = new ArrayList<User>();}
}
