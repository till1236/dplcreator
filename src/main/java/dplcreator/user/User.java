package dplcreator.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dplcreator.dpl.entity.Company;
import dplcreator.dpl.entity.Employee;
import org.hibernate.annotations.ManyToAny;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Propagation;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "user")
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    @JsonIgnore
    private String password;

    @Transient
    private Set<GrantedAuthority> authorities;

    @ManyToMany
    private List<Role> roles;

    @OneToOne(mappedBy = "user")
    private Employee employee;

    public User(){roles= new ArrayList<Role>();authorities=new HashSet<>();}
    public User(String _username, String _password, ArrayList<Role> _roles)
    {
        username = _username;
        password = _password;
        roles = _roles;
        authorities=new HashSet<>();
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
    public void addAuthorities(GrantedAuthority ga){authorities.add(ga);}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }
    public void addRole(Role r){roles.add(r);}

    public void setRoles(ArrayList<Role> roles) {
        this.roles = roles;
    }
}

