# DplCreator


DplCreator is a web application that let you create calendars("owned by a company"), add events("shift") 
to those and then add users ( "employees") to them.
This tool is currently usable but under development.

# Features

1. Create companies and add Users to them (they can login and then search for the company and enter or you can ann an employee)
2. Add day-based calendars to your companies
3. Add locations to your calendar
4. Add events/shifts to your calendar
5. Assign a employee/user of your company to the event

6. View calendar

# License

 Copyright &copy; Till Brand

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       https://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


